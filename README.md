# Google Apps Scripts

Google Apps Scripts 

## form2chat

A script to send new responses to a Google Form to a webhook of a Google Space.

### Installation

- Copy and paste the script into the Script Editor of the form.
- Insert the webhook URL of the Google Space webhook you created.
- Add a trigger as you need.
