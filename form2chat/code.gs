// The webhook URL of the Google Chat
var POST_URL = "";

function onSubmit(e) {
    var form = FormApp.getActiveForm();
    var allResponses = form.getResponses();
    var latestResponse = allResponses[allResponses.length - 1];
    var response = latestResponse.getItemResponses();
    var now = new Date();
    var widgets = [];

    for (var i = 0; i < response.length; i++) {
      
      var answer = './.';

      if (response[i].getItem().getType() == FormApp.ItemType.GRID) {
        var grid = response[i].getItem().asGridItem();
        answer = grid.getRows()[0] + ": " + response[i].getResponse()[0];
      } else if (response[i].getItem().getType() == FormApp.ItemType.CHECKBOX) {
        answer = response[i].getResponse().join(", ");
      } else {
        if (response[i].getResponse() != '') {
          answer = response[i].getResponse();
        }      
      }
      
      var widget = {
        "keyValue": {
            "topLabel": response[i].getItem().getTitle(),
            "content": answer,
            "contentMultiline": "true",
        }
      }

      widgets[i] = widget;
    }

    var payload = {
      "cards": [
        {
          "header": {
            "title": "Statusmeldung FFNI vom " + Utilities.formatDate(now, 'Europe/Berlin', 'dd.MM.yyyy'),
            "subtitle": "Gesendet von " + latestResponse.getRespondentEmail()
          },
          "sections": [
            {
              "widgets": widgets
            }
          ]
        }
      ]
    };

    var options = {
        "method": "post",
        "contentType": "application/json",
        "payload": JSON.stringify(payload)
    };

  UrlFetchApp.fetch(POST_URL, options);
};
